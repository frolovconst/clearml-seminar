import os
import random
import numpy as np
import pandas as pd
from clearml import Task
from tempfile import NamedTemporaryFile

os.environ['CLEARML_API_URL'] = 'https://api.clear.ml'
os.environ['CLEARML_WEB_URL'] = 'https://app.clear.ml'

PROJECT_NAME = f"ChatGPT"
run_name = "GTP5.5"

Task.force_requirements_env_freeze() # автосохранение pip freeze в clearml
task = Task.init(project_name=PROJECT_NAME,
                 task_name=run_name,
                )

task.close()
